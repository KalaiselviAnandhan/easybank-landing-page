let openmenu = false;

function showContent(){
    const menubtn = document.getElementById('menu');
    const nav = document.getElementById('nav');

    nav.classList.toggle("show");
    if(!openmenu){

        menubtn.classList.add('open');
        openmenu = true;
    }
    else{

        menubtn.classList.remove('open');
        openmenu = false;
    }
}